package apiUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
	private static XSSFSheet excelWSheet;
	private static XSSFWorkbook excelWBook;
	private static Cell cell;
	private static  Row row;
	final static String path_TestData = System.getProperty("user.dir") + "\\Resources\\";
	static final String file_TestData = "ER2.xlsx";
	static int columCount;

	// This method is to set the File path and to open the Excel file, Pass Excel
	// Path and Sheetname as Arguments to this method

	public static int setExcelFile() throws Exception {
		int rowcount = 0;
		System.out.println(rowcount);
		try {
			// Open the Excel file
			FileInputStream ExcelFile = new FileInputStream(path_TestData + file_TestData);
// Access the required test data sheet
			excelWBook = new XSSFWorkbook(ExcelFile);
			excelWSheet = excelWBook.getSheetAt(0);
			return rowcount = excelWSheet.getLastRowNum();

		} catch (Exception e) {
			throw (e);
		}

	}

	public void getRowNumber(int rowCount) throws Exception {
		int rowcount = setExcelFile();
		System.out.println(rowcount);
		row = excelWSheet.getRow(rowCount);
		if (row == null) {
			row = excelWSheet.createRow(rowCount);
		}
	}

	public void setCellResults(String value, int columCount) throws Exception {
		if (value == null) {
			cell = row.getCell(columCount);
			if (cell == null) {
				cell = row.createCell(columCount);
				cell.setCellValue(" ");
				cell = row.getCell(6);
				cell.setCellValue("No Results");
			} else {
				cell.setCellValue(" ");
				cell = row.getCell(6);
				cell.setCellValue("No Results");
			}
		} else {
			cell = row.getCell(columCount);
			if (cell == null) {
				cell = row.createCell(columCount);
				cell.setCellValue(value);
			} else {
				cell.setCellValue(value);
			}
		}

		FileOutputStream fileOut = new FileOutputStream(path_TestData + file_TestData);
		excelWBook.write(fileOut);
		fileOut.flush();
		fileOut.close();
	}
}
