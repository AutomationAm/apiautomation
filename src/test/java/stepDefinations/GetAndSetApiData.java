package stepDefinations;

import static io.restassured.RestAssured.given;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;

import apiUtils.ExcelUtil;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetAndSetApiData {
	static Response response;
	static ExcelUtil excelUtil = new ExcelUtil();
	static String file = System.getProperty("user.dir") + "\\Resources\\userList.txt";

	public static void main(String[] args) throws Exception {
		GetAndSetApiData getAndSetApiData = new GetAndSetApiData();
		int size = getAndSetApiData.getArryList();
		int rowCount = 1;

		for (int i = 0; i < size; i++) {
			String source_name = getAndSetApiData.getSource_nameFromIndex(i);

			String apiurl = getAndSetApiData.getApiURL(i);
			String mentorAPi = StringUtils.substringBefore(apiurl, "{");
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
				String line;
				line = br.readLine();

				while ((line = br.readLine()) != null) {
					excelUtil.getRowNumber(rowCount);
					excelUtil.setCellResults(source_name, 5);
					excelUtil.setCellResults(line, 0);
					getAndSetApiData.getMentorName(mentorAPi + line);
					rowCount++;
				}
				br.close();

			} catch (IOException e) { 
				System.out.println("ERROR: unable to read file " + file);
				e.printStackTrace();
			}

		}
	}

	public int getArryList() throws ParseException {
		 response = given().auth().basic("", "").when()
				.get("https://27vdiz3ixh.execute-api.eu-west-1.amazonaws.com/Prod/v1/id-index");
		String responseBody = response.getBody().asString();
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(responseBody);
		JSONArray ja = (JSONArray) json.get("results");
		return ja.size();
	}

	public void getMentorName(String api) throws Exception {
		System.err.println(api);
		try {
			Response response2 = given().auth().basic("", "").when().get(api);
			Assert.assertEquals(response2.getStatusCode(), 200);
			
			JsonPath jsonPath = response2.jsonPath();
			excelUtil.setCellResults(" ", 6);
			String nameValue = jsonPath.get("results[0].overview.name");
			excelUtil.setCellResults(nameValue, 1);
			String sourceURL = jsonPath.get("results[0].overview.sourceURL");
			excelUtil.setCellResults(sourceURL, 2);
			String birthDate = jsonPath.get("results[0].overview.birthDate");
			excelUtil.setCellResults(birthDate, 3);
			String nationality = jsonPath.get("results[0].overview.birthPlace.address.city");
			excelUtil.setCellResults(nationality, 4);
			
		} catch (Exception e) {
			excelUtil.setCellResults("No Results", 6);
		}
	}

	public String getSource_nameFromIndex(int value) {
		JsonPath jsonPath = response.jsonPath();
		String source_name = jsonPath.get("results[" + value + "].source_name");
		return source_name;
	}

	public String getApiURL(int value) {
		JsonPath jsonPath = response.jsonPath();
		String apiurl = jsonPath.get("results[" + value + "].apiurl");
		return apiurl;
	}
}
